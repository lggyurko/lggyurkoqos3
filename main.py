
"""
Server-side code for the queen of spades game. 
"""

import datetime
import logging
import os
import random
import re
import json
from google.appengine.api import channel
from google.appengine.api import users
#from google.appengine.ext import db
import webapp2
import jinja2
from google.appengine.api import memcache


expirationTime = 36000 # ten hour expiration

template_env = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.getcwd()))

numPs = 4
passmap = [[1,2,3,0],[3,0,1,2],[2,3,0,1]] 
passmapspec = [[1,2,3],[2,3,0],[3,0,1],[0,1,2]]
retries = 30

def memcache_op(retries,key,fn):
  memcache_client = memcache.Client()
  while retries > 0:
    retries -= 1
    game = memcache_client.gets(key)
    res = fn(game,key)
    game = res[0]
    #logging.warning('Memcache_op - retries.' + str(retries))
    #logging.warning('Memcache_op - game after creation.' + str(game.key) + str(game.users[0].nickname()))
    if memcache_client.cas(key,game):
      #logging.warning('Memcache_op - success.')
      return (True,res)
  logging.warning('Memcache_op - failed.')
  return (False,)

def attempt_to_add_player(game,key,user):
  logging.warning('Create_or_add -- game exists')
  newJoiner = game.addPlayer(user) 
  return (game, newJoiner)

def attempt_to_udpate_game(game,key,newGame):
  logging.warning('Update game.')  
  return (newGame,)


class Game():
  '''
  Simple struct to store game info. 
  '''
  def __init__(self,key):    
    self.key = key    
    self.users = []
    # the rest of the attributes are to be initialised on the client side    
    self.table = []
    # self.hands = numPs * [[]]
    self.selections = numPs * [[]]
    self.scores = numPs * [0]
    self.level = 0
    self.stage = 'idle'
    # self.nextplyid = 0

  def addPlayer(self,user):
    if user in self.users :
      # already in
      return False
    elif len(self.users) == 4:
      # not in, game full
      raise Exception('Game full, cannot add new player to it.')
    else:
      self.users.append(user)
      return True
    
def testGame1(user,game,level=4,nextId=1):      
  game.nextplyid = nextId
  game.level = level  
  game.stage = 'idle'
  game.table = []
  game.hands = [[1,2],[7,23],[12,20],[37,19]]
  game.selections = numPs*[[]]
  game.scores = [10,0,10,10]
  return game 
    

def get_token(user,key):
  return user.user_id() + key

class Updater():  
  #game = None # why class member, why not instance???

  def __init__(self, game):
    self.game = game

  def get_message(self,user,instruction = None):
    '''
    Parse game state to json object. 
    json objects are passed to clients through the channel.     
    '''    
    userPos = [i for i in range(len(self.game.users)) if self.game.users[i]==user][0]
    update = {
      'key' : self.game.key, 
      'me' : user.nickname(),     
      'players' : [u.nickname() for u in self.game.users],
    }    
    if hasattr(self.game,'table'):
      update['table'] = self.game.table
    if hasattr(self.game,'temptable'):
      update['temptable'] = self.game.temptable
    if hasattr(self.game,'hands'):
      update['hand'] = self.game.hands[userPos]
    if hasattr(self.game,'selections'):
      update['selected'] = self.game.selections[userPos]
    if hasattr(self.game,'scores'):
      update['scores'] = self.game.scores
    if hasattr(self.game,'level'):
      update['level'] = self.game.level
    if hasattr(self.game,'stage'):
      update['stage'] = self.game.stage
    if hasattr(self.game,'nextplyid'):
      update['next_player_id'] = self.game.nextplyid
    if hasattr(self.game,'results'):
      update['results'] = self.game.results
    update['instruction'] = '' if instruction == None else instruction
    logging.warning('update: ')
    logging.warning(json.dumps(update))
      

    return json.dumps(update)

  def send_update(self,instruction=None,userTo=None,userNotTo=None):
    '''
    Sends message to user(s).
    If 'user' is not None, only 'user' is sent the message, otherwise all. 
    '''        
    for u in self.game.users:      
      if (userTo == None or userTo == u) and (userNotTo == None or userNotTo != u):
        logging.warning('Message ' + instruction + ' being sent to ' + u.nickname())
        message = self.get_message(user=u,instruction=instruction)
        channel.send_message(get_token(u,self.game.key),message)    

  def join(self,user):
    '''
    Notify all clients of a new joiner. 
    '''
    logging.warning('Updater.add_player called with ' + user.nickname())    
    self.send_update(instruction='Newjoiner',userNotTo=user) # send update to rest


  def exchange(self,player,selection):
    '''
    Collects info on cards selected for echange.
    Once all four selected, hands are updates. 
    There is a bit too much control of server implemented here.
    Might need to revise this design issue in the future.
    '''
    numPsSelected = 0
    ind = None
    for i in range(numPs):
      if len(self.game.selections[i]) == 3 : 
        numPsSelected += 1
      if self.game.users[i] == player:
        self.game.selections[i] = selection
        ind = i
        numPsSelected += 1
    # update memcache, potential race condition
    #retries = 4
    fn = lambda g,k : attempt_to_udpate_game(g,k,self.game)
    success = memcache_op(retries,self.game.key,fn)
    if not success:
      raise Exception('Game update to memcache failed.')
    
    logging.warning('Number of players submitted selection: ' + str(numPsSelected))
    if numPsSelected == numPs:   
      logging.warning('Exchanging selections.')    
      for i in range(0,numPs):
        # i sender, j receiver
        if self.game.level < 3 :            
          j = passmap[self.game.level][i]            
          for c in self.game.selections[i]:
              # remove from sender                                 
              self.game.hands[i].remove(c)                
              # add to receover
              self.game.hands[j].append(c)
        else:
            for k in range(0,3):
              j = passmapspec[i][k] 
              c = self.game.selections[i][k]
              # remove from sender 
              self.game.hands[i].remove(c)
              # add to receover
              self.game.hands[j].append(c)
      for i in range(0,numPs):
        self.game.hands[i].sort()  
      self.game.stage = 'idle'
      # update game, no race condition here
      self.game.selections = numPs*[[]]  
      logging.warning(self.game)
      memcache.set(self.game.key,self.game)
      self.send_update(instruction='Exchange')

  def place_card(self,player,card,nextplyid):
    '''
    When card is placed, updates table and hands.
    '''
    self.updt_place_card(player,card,nextplyid)
    memcache.set(self.game.key,self.game)    
    self.send_update(instruction='Placedcard')
    logging.warning('Next player after place card ' + str(self.game.nextplyid))

  def updt_place_card(self,player,card,nextplyid):
    self.game.table.append(card) # append card to table
    for i in range(numPs) : # remove card from hand       
      if self.game.users[i] == player:
        self.game.hands[i].remove(card)
    self.game.nextplyid = nextplyid

  def clean_table(self,player,card,score,nextplyid):
    logging.warning('Next player at clean table' + str(self.game.nextplyid))
    self.updt_clean_table(player,card,score,nextplyid)    
    memcache.set(self.game.key,self.game)
    #self.send_update(instruction='CleanedTable',userNotTo=user)
    self.send_update(instruction='CleanedTable')

  def updt_clean_table(self,player,card,score,nextplyid):
    self.updt_place_card(player,card,nextplyid)
    self.game.scores[self.game.nextplyid] = score
    self.game.temptable = self.game.table
    self.game.table = []    

  def updt_deal(self,player,level,nextplyid,card,score,winnerid): 
    if winnerid:
      self.updt_clean_table(player,int(float(card)),int(float(score)),int(float(winnerid)))
    self.game.level = level
    self.game.nextplyid = nextplyid
    deck = range(52)
    random.shuffle(deck)
    self.game.hands = []    
    for i in range(numPs):
      hand = deck[(i*13):((i+1)*13)]
      hand.sort()
      self.game.hands.append(hand)
    self.game.stage = 'inselection' 

  def deal(self,player,level,nextplyid,card,score,winnerid): 
    '''
    Generates hands, and updates clients
    '''
    self.updt_deal(player,level,nextplyid,card,score,winnerid) 
    memcache.set(self.game.key, self.game)
    logging.warning('Dealt ' + str(self.game.hands))
    # update all
    self.send_update(instruction='Deal')    

  def final_results(self,player,card,score,winnerid,results): 
    self.updt_clean_table(player,card,score,winnerid)    
    logging.warning('Final results: ' + results)
    self.game.results = results
    memcache.set(self.game.key, self.game)
    self.send_update(instruction='FinalResults')

  def reset(self):
    self.game.scores = numPs * [0] # set scores to zero    
    memcache.set(self.game.key,self.game)
    self.deal(0,0)
  
class GameFromRequest():
  '''
  Extracts game from memcache
  '''
  #game = None # why class member, why not instance???

  def __init__(self, request):
    user = users.get_current_user()
    key = request.get('k')
    if user and key:
      self.game = memcache.get(key)
      if self.game == None:
        logging.warning('Data item not found. Key: ' + str(key))

  def get_game(self):
    return self.game



#***********************************************************
#***********************************************************
#***********************************************************
# EVENTHANDLERS
#***********************************************************
#***********************************************************
#***********************************************************


class HandleJoin(webapp2.RequestHandler):
  '''
  Handles /join requests - updates other existing players on new joiner. 
  '''
  def post(self):
    game = GameFromRequest(self.request).get_game()
    user = users.get_current_user()
    if game != None and user:      
      Updater(game).join(user) 


class HandleSelection(webapp2.RequestHandler):
  '''
  Handles /selection requests
  '''
  def post(self):
    game = GameFromRequest(self.request).get_game()
    user = users.get_current_user()
    if game != None and user:
      # extract selection
      selection = []      
      selection.append(int(float(self.request.get('s0'))))
      selection.append(int(float(self.request.get('s1'))))
      selection.append(int(float(self.request.get('s2'))))
      Updater(game).exchange(user,selection)

class HandleDeal(webapp2.RequestHandler):
  '''
  Handles /deal requests
  '''
  def post(self):
    game = GameFromRequest(self.request).get_game()    
    user = users.get_current_user()
    card = self.request.get('c')    
    score = self.request.get('sc')
    winnerid = self.request.get('wp')
    nextplyid = int(float(self.request.get('np')))
    level = int(float(self.request.get('l')))    
    if game != None and user != None:      
      Updater(game).deal(user,level,nextplyid,card,score,winnerid) 
    

class HandlePlaceCard(webapp2.RequestHandler):
  '''
  Handles /placecard requests
  '''
  def post(self):
    game = GameFromRequest(self.request).get_game()
    user = users.get_current_user()
    card = int(float(self.request.get('c')));
    nextplyid = int(float(self.request.get('np')))
    if game != None and user:      
      Updater(game).place_card(user,card,nextplyid) 

class HandleCleanTable(webapp2.RequestHandler):
  '''
  Handles /cleantable requests
  '''
  def post(self):
    game = GameFromRequest(self.request).get_game()
    user = users.get_current_user()
    card = int(float(self.request.get('c')));
    score = int(float(self.request.get('sc')))
    nextplyid = int(float(self.request.get('np')))    
    if game != None and user:      
      Updater(game).clean_table(user,card,score,nextplyid) 

class HandleFinalResults(webapp2.RequestHandler):
  '''
  Handles /finalresults requests
  '''
  def post(self):
    game = GameFromRequest(self.request).get_game()
    user = users.get_current_user()
    card = int(float(self.request.get('c')))    
    score = int(float(self.request.get('sc')))
    winnerid = int(float(self.request.get('wp')))    
    results = self.request.get('win')       
    if game != None and user:      
      Updater(game).final_results(user,card,score,winnerid,results) 

class HandleReset(webapp2.RequestHandler):
  '''
  Handles /reset requests
  '''
  def post(self):
    game = GameFromRequest(self.request).get_game()      
    if game != None and user:      
      Updater(game).reset() 

class HandleMain(webapp2.RequestHandler):
  """The main UI page, renders the 'index.html' template."""

  def get(self):
    """Renders the main page. When this page is shown, we create a new
    channel to push asynchronous updates to the client."""
    user = users.get_current_user()
    # special case: if f defined, flush memcache
    flush = self.request.get('f')
    if user :
      if flush and user.nickname() in ['lggyurko@googlemail.com']: 
        memcache.flush_all()
        self.response.out.write('Memcache flushed.')
        return

    # get game key
    key = self.request.get('k')   
    master_key = str(15956279893);

    level = self.request.get('l')
    nextId = self.request.get('n')
    if level and nextId:
      game = memcache.get(key)
      game = testGame1(user,game,level=int(float(level)),nextId=int(float(nextId))) 
      logging.warning('Test game:')
      logging.warning(str(game.users))
      logging.warning(str(game.scores))
      logging.warning(str(game.hands))
      logging.warning(str(game.level))
      memcache.set(key,game)


    if user:
      if key:  
        # check if key is valid
        valid_key = memcache.get(master_key);
        if valid_key:
          if key == valid_key:
            logging.warning('Key is valid.')            
            #retries = 4
            fn = lambda g,k : attempt_to_add_player(g,k,user)
            (success,res) = memcache_op(retries,key,fn)
            if not success : 
              self.response.out.write('Game access failed. Please try again later.')
              return
            instruction = None
            if res[1] == True : # added as newJoiner
              instruction = 'Initialise'
            else:
              instruction = 'Refresh'
            game = memcache.get(key)
            # set up game page  
            token = channel.create_channel(get_token(user,key))
            template_values = {'token': token,                                                          
                              'initial_message': Updater(game).get_message(user,instruction=instruction)
                              }
            template = template_env.get_template('game.html')
            self.response.out.write(template.render(template_values))
            return     
          else:
            self.response.out.write('Thank you for visiting. Please contact the page owner for a valid game key.') 
            return
      elif user.nickname() in ['lggyurko@googlemail.com']:
        # generate new valid key
        key = str(random.randrange(10e6));
        memcache.set(master_key,key)
        game = Game(key)
        memcache.set(key,game)
        self.response.out.write('<a href="/?k='+key+'">Url to a valid game.</a>') 
        pass
      else :
        self.response.out.write('Thank you for visiting. Please contact the page owner for a valid game key.') 
        return
    else: 
      # redirect user to login page
      self.redirect(users.create_login_url(self.request.uri))
      return


application = webapp2.WSGIApplication([
    ('/', HandleMain),
    ('/joined', HandleJoin),
    ('/selection', HandleSelection),
    ('/deal', HandleDeal),
    ('/placecard', HandlePlaceCard),
    ('/cleantable', HandleCleanTable),    
    ('/finalresults', HandleFinalResults),
    ('/reset', HandleReset)],
    debug=True)


