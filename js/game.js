// This file contains the functions that update the logical state of the game.
// Design principles: 
// -- client can observe changes in the state of the game
// -- when a state of change is observed, the client prepares the 
//    information to be shared with the other clients, 
//    then sends it to the server.
// -- the server keeps tract of the state of the game, and communicates changes
//    except for two special events: exchange selections, deal. 
// -- exchange selections: server collects selections, than sorts out the exchange,
//    clients does not get to see the selections that they are not supposed to.
// -- deal: server sets up hands, clients only get access to their own hand. 

// global state of client
var state = {};

//Some global constants. 
var trumps = ['clubs', 'diamonds', 'hearts', 'spades', 'sans'];
var pass_dirs = ['pass them forward','to pass them backward',
                 'pass them across', 
                 'spread them (in the order of selection)', 
                 'spread them (in the order of selection)'];
var stages = ['idle','inselection','ingame'];
var suites = trumps.slice(0,4);
var faces = ['2','3','4','5','6','7','8','9','10','Jack','Queen','King','Ace'];
var cards = []
for (var i=0; i<suites.length; i++)
  for(var j=0; j<faces.length; j++)
    cards.push(suites[i]+'_'+faces[j]);
//console.log(cards);

var scoreTable = function(table){
  // scores a table according to the rules of Queen of spades
  var score = 10;
  for (i=0; i<4; i++){
    var card = cards[table[i]].split("_");
    if (card[0] === 'hearts'){
      score -= (faces.indexOf(card[1]) + 2);
    }
    else if (card[0] === 'spades' && card[1] === 'Queen'){
      score -= 26;
    }
  }
  return score;
};

var suiteInHand = function(suite,hand){
  // determines if there is at least one card in hand that has a given suite in hand
  // used in validPlacement
  for (var i=0; i<hand.length; i++){
    var card = cards[hand[i]].split('_');
console.log(suite === card[0]);    
console.log('hand i:' + cards[hand[i]]);
    if(suite === card[0])
      return true;
  }
  return false;
};

var validPlacement = function(table,hand,card){
// validates placement of a card
  if(table.length == 0)
    return true;
  var card0 = cards[table[0]].split('_');
  var card1 = cards[card].split('_');
//console.log('Suite in hand: ');
//console.log(suiteInHand(card0[0],hand));
  if (card0[0] !== card1[0] && suiteInHand(card0[0],hand))
    return false;
  return true;
};

var cardCompare = function(card1,card2,trump){
  // compares cards, used in tableWinnerId
  if (card1[0] === card2[0]){
    return faces.indexOf(card1[1]) < faces.indexOf(card2[1]);
  }
  if (card2[0] === trump){
    return true;
  }
  return false;
};

var tableWinnerId = function(table,trump){ // relative to caller
// determines who takes the table when it is full
  var card = cards[table[0]].split('_');
  var ind = 0;
  for (var i=1; i<table.length; i++){
    var cardi = cards[table[i]].split('_');
    if(cardCompare(card,cardi,trump)){
      card = cardi;
      ind = i;
    }
  }
  return ind;
};

var get_pass_dir = function(state){
  return pass_dirs[state.level];
};

var get_trump = function(state){
  return trumps[state.level];
};

var get_select_button_on = function(state){
  return state.stage === stages[1] && state.selected.length === 3;
};

var get_text_stage = function(state){
  // returns desctiption of stage from state
  if (state.stage === stages[2]) // in game
    return 'Your turn!';
  if (state.stage === stages[1] && state.selected.length < 3) // in selection
    return 'Select three cards to ' + pass_dirs[state.level];
  if (state.stage === stages[1] && state.selected.length === 3) // in selection, ready to submit
    return '';
  if (state.stage === stages[0] && state.selected.length === 0 && state.players.length === 4)  // selection submitted
    return 'Awaiting ' + state.players[state.next_player_id] + ' to place a card.';
  if (state.stage === stages[0] && state.selected.length === 3)  // selection submitted
    return 'Selection submitted. Awaiting other players to submit their selection.';  
  if (state.stage === stages[0] && state.players.length < 4) // game hasn't started
    return 'Awaiting players';
  return 'Invalid stage, debug your code!';
};

var set_game_at_initialise = function(newstate){
  // set game when sever initialises a client in a game
    state.players = newstate.players;
    state.me = newstate.me;
    state.key = newstate.key;
    state.next_player_id = 0;
    state.stage  = stages[0];
    state.table = [];
    state.selected = [];
    state.level = 0;
    state.scores = [0,0,0,0];
    state.text_stage = get_text_stage(state);
    state.select_button_on = get_select_button_on(state);
    state.trump = get_trump(state);  
    return state;
};

var set_game_at_newjoiner_msg  = function(newstate){
  // set game when sever informs client of a newjoiner
  state.players = newstate.players;
  return state;
};


var set_game_from_newstate = function(newstate){
// set game a newstate is seceived from server
console.log('nestate in game from newstate');
console.log(newstate);

  set_game_at_initialise(newstate);

  state.next_player_id = newstate.next_player_id || state.next_player_id;  
  state.table = newstate.table || state.table;
  state.hand = newstate.hand || state.hand;
  state.players = newstate.players || state.players;
  state.selected = newstate.selected || state.selected;
  state.level = newstate.level || state.level;
  state.scores = newstate.scores || state.scores;

  if(state.selected.length === 3)
    state.stage = stages[0]; // selected already, waiting for others
  else if (newstate.stage === stages[1])
    state.stage = stages[1] // in selection
  else if (state.me === state.players[state.next_player_id])
    state.stage = stages[2] // in game
  else 
    state.stage = stages[0] 

console.log('Stage at refresh: ' + state.stage);
  if(newstate.results != undefined)
    state.text_stage = newstate.results;
  else  
    state.text_stage = get_text_stage(state);
  state.select_button_on = get_select_button_on(state);
  state.trump = get_trump(state);
console.log('State at refresh: ');
console.log(state);
  return state;
};


var set_game_at_placedcard_msg = function(newstate){
// set game when a placedcard msg is seceived from server
  state.table = newstate.table;
  state.next_player_id = newstate.next_player_id;
console.log('State after card placed.');
console.log(state);       
  if (state.me === state.players[state.next_player_id])
    state.stage = stages[2];
  else
    state.stage = stages[0];
  state.text_stage = get_text_stage(state); 
  return state;
};

var set_game_at_cleanedtable_msg = function(newstate){
// set game when a cleanedtable msg is seceived from server
  state.next_player_id = newstate.next_player_id;
  if (state.me === state.players[state.next_player_id])
    state.stage = stages[2];
  else
    state.stage = stages[0]
  state.temptable = newstate.temptable;
  state.scores = newstate.scores;
  state.level = newstate.level;    
  state.table = [];
  state.text_stage = get_text_stage(state) 
  return state;
};

var set_game_at_finalresults_msg = function(newstate){
  // set game when a finalresults msg is seceived from server
    state.temptable = newstate.temptable;
    state.scores = newstate.scores;
    state.table = [];
    return state;
};

var set_game_at_deal_msg = function(newstate){
  // set game when a deal msg is seceived from server
console.log('sgad - state before') ;
console.log(state);
  if (newstate.temptable != undefined){
    state.temptable = newstate.temptable;
    state.scores = newstate.scores;
  }
  state.players = newstate.players;
  state.table = [];  
  state.level = newstate.level
  state.next_player_id = newstate.next_player_id
  state.hand = newstate.hand; // add ordering later
  state.trump = get_trump(state);
  state.stage = stages[1];
  state.text_stage = get_text_stage(state);
  state.select_button_on = get_select_button_on(state);
  state.selected = [];
console.log('sgad - state after') ;
console.log(state);
  return state;
};

var set_game_at_selection_submitted_by_me = function(){
  // set game when selection is submitted to server by client
  if(state.selected.length === 3){
    state.stage = stages[0];
    state.select_button_on = get_select_button_on(state);
    state.text_stage = get_text_stage(state);
  }
  return state;
};

var set_game_at_exchange_msg = function(newstate){
  // set game when an exchange (selections) msg is seceived from server
  state.hand = newstate.hand;
  state.selected = newstate.selected;
  if (state.players[state.next_player_id]==state.me)
    state.stage = stages[2];
  else 
    state.stage = stages[0];
  state.select_button_on = get_select_button_on(state);
  state.text_stage = get_text_stage(state);
  return state;
};

var set_game_at_hand_clicked_by_me = function(card){
  // set game when a card in hand is clicked on by client
console.log('Set game at hand clicked called.');
  state.update = '';
  if (state.stage === stages[0]){
    // do nothing
  }
  else if (state.stage === stages[1]){
    // in selection
    var i = state.selected.indexOf(card);
console.log('Clicked card: ' + cards[card]);  
console.log('Index of card clicked i selected: ' + i.toString());  
console.log('Selected so far: ');
console.log(state.selected);
console.log('length ' + state.selected.length.toString());
    //check if it is selected
    if(i===-1){
      if(state.selected.length<3) 
        state.selected.push(card);//add to selected
      // else do nothing
    }
    else { // remove selected
      state.selected.splice(i,1);//remove from selected
console.log('Card removed');
console.log(state.selected);
    }
  
    if (state.selected.length===3){
      state.select_button_on = true;
      state.text_stage = get_text_stage(state);
    }
    else {
      state.select_button_on = false;
      state.text_stage = get_text_stage(state);
    }
  }
  else if (state.stage === stages[2]){
    if(set_game_at_clicked_in_game_by_me(card)){
console.log('sgahc - Client placed a valid card.');
      //send_placed_card_to_server(state);
      state.update = 'placecard';
      state.stage = stages[0]; 
      //state.updateServer = true;
        if(state.table.length == 4){
console.log('sgahc - Client detected table to be cleaned.');
          state = set_game_when_detecting_full_table(state);
          state.update = 'cleantable';
          //send_score_and_nextpi_to_server(state);
          if(state.hand.length == 0 && state.level < 5){
console.log('sgahc - Client detected end of round.');
            state = set_game_when_detecting_round_end(state);
            state.update = 'deal';
            //request_deal_from_sever();
          }
          if(state.level == 5) {// game's over
console.log('sgahc - Client detected game over.');
              state = set_game_when_detecting_game_end(state);
              state.update = 'endgame';
              //send_final_results_to_server(state);
          }
        }
    }
    else 
      state.text_stage = 'Invalid card! Please select another one.'
  }
  return state;
};

var set_game_at_clicked_in_game_by_me = function(card){
  // set game when 'in game' state a card in hand is clicked
console.log('Card clicked in game: ' + cards[card]);
  // if valid placement update hand, table, next_player
  if(validPlacement(state.table,state.hand,card)){
    state.hand.splice(state.hand.indexOf(card),1);
    state.table.push(card);
    state.next_player_id = (state.next_player_id + 1) % 4;
    state.stage = stages[0];
    state.text_stage = get_text_stage(state);
    return true;
  }
  return false;
};


var set_game_when_detecting_full_table = function(state){  
  // set game when client detects full table
console.log('sgatc - state before');
console.log(state);  
  // update next player id
  var relId = tableWinnerId(state.table,trumps[state.level]);
  var absId = (state.next_player_id + relId ) % 4;
  state.next_player_id = absId;  
  // update scores
  state.winnerId = absId;  
  state.scores[absId] += scoreTable(state.table);
console.log('Taker id: '+ absId.toString());
console.log('Score: '+ scoreTable(state.table).toString() + ' in total: ' + state.scores[absId].toString());
console.log(state.scores);
console.log('sgatc - state after');
console.log(state);
  return state;
};

var set_game_when_detecting_round_end = function(state){
  // set game when client detects end of round
console.log('sgare - state before');  
console.log(state)  
  state.level += 1;
  state.next_player_id = state.level % 4;
  //state.text_stage = get_text_stage(state);  // redundant
console.log('sgare - state after');  
console.log(state)
  return state;
};

var set_game_when_detecting_game_end = function(state){
  // set game when client detects end of game
console.log('sgae - state before') ;
console.log(state);    
  state.stage = stages[0];
  var score = state.scores[0];
  var prefix = 'The winner is ';
  var winner = state.players[0];
  for(var i=1;i<4;i++){
    if(state.scores[i] == score){
      prefix = 'The winners are ';
      winner += ', ' + state.players[i];
    }
    else if (state.scores[i] > score){
      prefix = 'The winner is ';
      winner = state.players[i];
      score = state.scores[i];
    } 
  }
  state.winner = prefix + winner;
console.log('sgae - state after');
console.log(state);
  return state;
};



