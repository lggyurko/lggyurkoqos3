//This file containes the functions that directly act on the page elements. 
var cardwidth = 150;
var tabletop = 30;
var tableedge = 2*cardwidth;
var xstep = 70;
var ystep = 5;
var ymove = 10;
var yymove = 50;

var add_view_table = function(){
console.log('Add view table called.');
    // creating slots for cards on table
   for (var i = 0; i < 4; i++){
      var img = $("<img>");            
      img.attr('id', 'table' + i.toString());                 
      img.appendTo($("#table"));
   }
   $("#table img").each(function() {
      $(this).hide();
      var posx = $(this).index() * cardwidth + tableedge;
      var posy = tabletop;      
      $(this).animate({
         left: posx.toString() + "px",
         top: posy.toString() + "px",         
      }, 1);      
   });
};

var spread_cards_in_hand = function(){
   var pos = $("#hand").position();
  $("#hand img").each(function() {
      var posx = $(this).index() * xstep + pos.left;
      var posy = $(this).index() * ystep + pos.top;
      if ( $(this).hasClass('selected')){
        posy -= yymove;
      }
      $(this).animate({
         left: posx.toString() + "px",
         top: posy.toString() + "px"
      }, 1);
   });
};

var add_view_hand = function(){
console.log('Add view hand called.');
    // creating slots for cards in hand
   for (var i = 0; i < 13; i++) {
      var img = $("<img>");
      img.attr('src',card_urls['back']);      
      img.attr('id', 'back' + i.toString());      
      img.appendTo($("#hand"));
   } 
   spread_cards_in_hand();
};

var set_view_me = function(me){
    // sets the 'me' element
  console.log('Set view me called: ')
  console.log(me);
    $("#me").html("User: " + me);
    
};

var set_view_players = function(players){
    // sets the list of players
  console.log('Set view players called: ')
  console.log(players);
    if (players != undefined){
        for(var i=0; i< players.length; i++){
            var id = "#player" + i.toString();
            $(id).html(players[i]);
        }
    }
};

var set_view_scores = function(scores){
    // sets the list of scores
  console.log('Set view scores called: ')
  console.log(scores);
    for(var i=0; i<4; i++){
        var id = "#score" + i.toString();
        $(id).html(scores[i]);
    }
};

var set_view_table = function(table){
    // sets the view of the table
  console.log('Set view table called: ')
  console.log(table);
    var len = table.length;  
    $("#table img").each(function() {      
        if ($(this).index() < len){        
            $(this).show();
            $(this).attr("src", card_urls[cards[table[$(this).index()]]]);        
  console.log(table[$(this).index()]);   
            $(this).attr("id",table[$(this).index()]);
        }
        else {        
            $(this).hide();
        }
  });
};

var set_view_hand = function(hand){
    // sets the view of the table
  console.log('Set view hand called: ')
  console.log(hand);
    var len = hand.length;  
    $("#hand img").each(function() {      
        if ($(this).index() < len){        
            $(this).show();
            $(this).attr("src", card_urls[cards[hand[$(this).index()]]]);        
            $(this).attr("id",hand[$(this).index()].toString());            
  //console.log(hand[$(this).index()]);      
        }
        else {        
            $(this).hide();
        }
  });
};

var set_view_trump = function(trump){
    // sets the trump
  console.log('Set view trump called: ')
  console.log(trump);
    $("#trump").attr('src',card_urls[trump]);
};

var set_view_text_stage = function(text_stage){
  console.log('Set view text_stage called: ')
  console.log(text_stage);
    if (text_stage === '')
        $("#text_stage").hide();
    else {
        $("#text_stage").show();
        $("#text_stage").html(text_stage);
    }
};

var set_view_select_button = function(select_button_on){
  console.log('Set view select_button called: ')
  console.log(select_button_on);
    if(select_button_on)
        $("#select_button").show();
    else
        $("#select_button").hide();
};

var set_view_selected = function(selected){
    $("#hand img").each(function() {        
        var id = parseInt($(this).attr('id'));
        if (state.selected.indexOf(id) === -1 && $(this).hasClass('selected'))
        {
           $(this).removeClass('selected');
        }
        else if (state.selected.indexOf(id) !== -1 && ! $(this).hasClass('selected') )
        {
           $(this).addClass('selected');
        }
    });
};

var vertical_move = function(obj, step) {   
   var pos = obj.position();
   pos.top += step;
   obj.animate({
      top: pos.top.toString() + "px"
   }, 25);
};

var onHoverOnHand = function () {
    if(! $(this).hasClass('selected')){
      vertical_move($(this), (-1)*ymove);
    }
};

var onHoverOffHand = spread_cards_in_hand;

var onClickHand = function(){
    // get card then
  console.log('onClickHand');
    var card = parseInt($(this).attr('id'));
    set_view_at_hand_clicked(card);
};

var onClickSelected = function(){
  console.log('Submit selected clicked.')
    set_view_at_selection_submitted();
}

var set_listeners = function(){
console.log('Listeners being set.')
  $("#hand img").hover(onHoverOnHand, onHoverOffHand);
  $("#hand img").click(onClickHand);
  $("#submit_selected").click(onClickSelected);

   // demos , hidden in final version
  $("#demo1").click(demo_in_game);
  $("#demo2").click(demo_in_selection);
  $("#demo3").click(demo_clear_table);
  $("#demo4").click(demo_round_end);
  $("#demo5").click(demo_game_end);

};
