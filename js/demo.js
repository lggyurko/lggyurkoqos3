// ****************** DEMO *********************
var demo_in_game = function() {
  // general view in game
  console.log('Demo called');
    var state = {};
    var handStr = ['spades_Queen','clubs_3', 'clubs_10','clubs_5', 'hearts_Jack', 'hearts_Ace','diamonds_10'];
    var hand = [];
    for(var i=0; i<handStr.length;i++)
      hand.push(cards.indexOf(handStr[i]));
    state.hand = hand;
    var table = [cards.indexOf('spades_Ace'), cards.indexOf('clubs_4')];
    state.table = table;
    state.players = ['Mr White', 'Mr Pink', 'Mr Brown', 'Mr Orange'];
    state.scores = [0,1,2,3];
    state.stage = stages[2];
    state.text_stage = 'Your turn!';
    
    set_view_at_start('Demo',state);
};


var demo_in_selection = function() {
  // in selection
  console.log('Demo called');
    var state = {};
    var handStr = ['spades_Queen','clubs_3', 'clubs_10','clubs_5', 'hearts_Jack', 'hearts_Ace','diamonds_10',
                    'spades_10', 'spades_King','diamonds_Jack','diamonds_Ace','clubs_Ace','clubs_7'];
    var hand = [];
    for(var i=0; i<handStr.length;i++)
      hand.push(cards.indexOf(handStr[i]));
    
    state.hand = hand;
    var table = [];
    state.table = table;
    state.players = ['Mr White', 'Mr Pink', 'Mr Brown'];
    state.scores = [0,1,2,3];
    state.stage = stages[1];
    state.level = 1;
    state.text_stage = 'Select three cards ' + get_pass_dir(state);
    
    set_view_at_start('Demo',state);
};

var demo_clear_table = function() {
  // in selection
  console.log('Demo called');
    var state = {};
    var handStr = ['spades_Queen','clubs_3', 'clubs_10','clubs_5', 'hearts_Jack', 'hearts_Ace','diamonds_10'];
    var hand = [];
    for(var i=0; i<handStr.length;i++)
      hand.push(cards.indexOf(handStr[i]));
    state.hand = hand;
    var table = [cards.indexOf('spades_Ace'), cards.indexOf('clubs_4'), cards.indexOf('clubs_7')];
    state.table = table;
    state.players = ['Mr White', 'Mr Pink', 'Mr Brown','Mr Orange'];
    state.scores = [0,0,0,0];
    state.stage = stages[2];
    
    set_view_at_start('Demo',state);
};

var demo_clear_table = function() {
  // in selection
  console.log('Demo called');
    var state = {};
    var handStr = ['spades_Queen','clubs_3', 'clubs_10','clubs_5', 'hearts_Jack', 'hearts_Ace','diamonds_10'];
    var hand = [];
    for(var i=0; i<handStr.length;i++)
      hand.push(cards.indexOf(handStr[i]));
    state.hand = hand;
    var table = [cards.indexOf('spades_Ace'), cards.indexOf('clubs_4'), cards.indexOf('clubs_7')];
    state.table = table;
    state.players = ['Mr White', 'Mr Pink', 'Mr Brown','Mr Orange'];
    state.scores = [0,0,0,0];
    state.stage = stages[2];
    
    set_view_at_start('Demo',state);
};

var demo_round_end = function() {
  // in selection
  console.log('Demo called');
    var state = {};
    var hand = [cards.indexOf('spades_Queen')];
    state.level = 3;
    state.hand = hand;
    var table = [cards.indexOf('spades_Ace'), cards.indexOf('clubs_4'), cards.indexOf('clubs_7')];
    state.table = table;
    state.players = ['Mr White', 'Mr Pink', 'Mr Brown','Mr Orange'];
    state.scores = [0,0,0,0];
    state.stage = stages[2];
    
    set_view_at_start('Demo',state);
};

var demo_game_end = function() {
  // in selection
  console.log('Demo called');
    var state = {};
    var hand = [cards.indexOf('spades_Queen')];
    state.hand = hand;
    state.level = 4;
    var table = [cards.indexOf('spades_Ace'), cards.indexOf('clubs_4'), cards.indexOf('clubs_7')];
    state.table = table;
    state.players = ['Mr White', 'Mr Pink', 'Mr Brown','Mr Orange'];
    state.scores = [0,0,0,0];
    state.stage = stages[2];
    
    set_view_at_start('Demo',state);
};

