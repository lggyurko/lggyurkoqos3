// This file contains all the functions that mediate between the view and the model

var add_view_all = function(state){
console.log('Add view all called.');
    add_view_table();
    add_view_hand();
};

var set_view_all = function(state,withHands){    
    set_view_me(state.me);
    set_view_players(state.players);
    set_view_scores(state.scores);
    set_view_table(state.table);
    set_view_trump(state.trump)
    set_view_text_stage(state.text_stage);
    set_view_select_button(state.select_button_on);
    set_view_selected(state.selected);
    if (state.hand != undefined)
        set_view_hand(state.hand);
};

var set_view_at_initialise = function(newstate){
// set view when a client is initialised in a game
console.log('Set view at initialise called.');
    state = set_game_at_initialise(newstate,me);
    add_view_all();
    set_view_all(state,false);

    // request deal if me is the fourth player
console.log('state after initialisation:')
console.log(state)
console.log('length: ' + state.players.length.toString())
    if (state.players.length === 4){
console.log('state.players[3] vs state.me')
console.log(state.players[3])
console.log(state.me)
        if (state.players[3] === state.me ){
            setTimeout(function(){request_deal_from_sever();},1000); 
console.log("Fourth player joined, reqest deal. ")
        }        
    }
    else
        setTimeout(function(){sendMessage('/joined');},1000);
};

var set_view_at_refresh_msg = function(newstate){
// set view when state is refreshed
console.log('newstate at refresh:');
console.log(newstate);
    state = set_game_from_newstate(newstate);//set_game_at_refresh(newstate);
    add_view_all();
    set_view_all(state);
};

var set_view_at_newjoiner_msg = function(newstate){
// set view when a newjoiner is added
    //var state = set_game_from_newstate(newstate);
    var state = set_game_at_newjoiner_msg(newstate);
    set_view_players(state.players);
};

var set_view_at_hand_clicked = function(card){
// set view when client clicked at a hand   
console.log('Set view at hand clicked called. State:');
    var state = set_game_at_hand_clicked_by_me(card); 
console.log(state);
    set_view_selected(state.selected);
    set_view_text_stage(state.text_stage);
    set_view_select_button(state.select_button_on);    
    spread_cards_in_hand();
console.log(state);
    if(state.update === 'placecard'){
console.log('update=placecard');
        set_view_hand(state.hand);
        send_placecard_to_server(state);
    }
    else if (state.update === 'cleantable'){
console.log('update=cleantable');
        set_view_hand(state.hand);
        send_cleantable_to_server(state);
    }
    else if (state.update === 'deal'){
console.log('update=deal');
        send_deal_to_server(state);
    }
    else if (state.update === 'endgame'){
console.log('update=endgame');
        set_view_hand(state.hand);
        send_endgame_to_server(state);
    }
    else {
console.log('Undefined action');
    }
};

var set_view_at_selection_submitted = function(){
// set view when selection is submitted to server    
    var state = set_game_at_selection_submitted_by_me();
    set_view_text_stage(state.text_stage);
    set_view_select_button(state.select_button_on);
    send_selected_to_server(state);
};

var set_view_at_exchange_msg = function(newstate){
// set view when exchange msg is received from server    
    var state = set_game_at_exchange_msg(newstate);
    set_view_text_stage(state.text_stage);
    set_view_select_button(state.select_button_on);    
    set_view_selected(state.selected);
    set_view_hand(state.hand);
    spread_cards_in_hand();
};

var send_selected_to_server = function(state){
// inform server when client submits his/her selection for exchange
console.log('Selected sent to server.'); 
    var selection = ''
    for(var i=0; i<3; i++){
        selection += '&s' + i.toString() + '=' + state.selected[i].toString();    
    }
    sendMessage('/selection',selection);
};

var send_endgame_to_server = function(state){
// inform server when client end of game     
    var msg = "&c=" + state.table[state.table.length-1].toString();
    msg += "&sc=" + state.scores[state.winnerId].toString();     
    msg += "&wp=" + state.winnerId.toString();
    msg += "&win=" + state.winner;    
    sendMessage('/finalresults',msg);
};

var send_deal_to_server = function(state){
// inform server when client end of round 
    var msg = "&c=" + state.table[state.table.length-1].toString();
    msg += "&sc=" + state.scores[state.winnerId].toString();     
    msg += "&wp=" + state.winnerId.toString();
    msg += "&l=" + state.level.toString();
    msg += "&np=" + state.next_player_id.toString();
    sendMessage('/deal',msg);
};

var send_cleantable_to_server = function(state){
// inform server when client detects full table
//next player is the winner --> this is inconsistent with the other case
//and it is a potential source of error
    var msg = "&c=" + state.table[state.table.length-1].toString();
    msg += "&sc=" + state.scores[state.next_player_id].toString();     
    msg += "&np=" + state.next_player_id.toString();
    sendMessage('/cleantable', msg);  
};

var send_placecard_to_server = function(state){  
// informs server when the client places a card on the table
console.log('Update server with placed card.');    
    var msg = "&c=" + state.table[state.table.length-1].toString();
    msg += "&np=" + state.next_player_id.toString();    
    sendMessage('/placecard', msg);
};

var request_deal_from_sever = function(){
// send deal request to server
console.log('Request deal from server.');    
    var msg = "&l=" + state.level.toString();
    msg += "&np=" + state.next_player_id.toString();
    sendMessage('/deal',msg);
};

var set_view_at_deal_msg = function(newstate){  
// set game when a deal msg is received from server
    var state = set_game_at_deal_msg(newstate);
console.log('Hand after server dealt.');
console.log(state.hand);
console.log('Set view after server dealt.');    
    set_view_text_stage(state.text_stage);
    set_view_select_button(state.select_button_on);
    set_view_trump(state.trump);
    set_view_scores(state.scores);
    set_view_players(state.players);
    if (state.temptable != undefined)
        set_view_table(state.temptable);
    setTimeout(function(){set_view_table(state.table);},3000);
    //setTimeout(function(){set_view_hand(state.hand);},3000);
    set_view_hand(state.hand);
};

var set_view_at_placecard_msg = function(newstate){
// set game when a placecard msg is received from server
    var state = set_game_at_placedcard_msg(newstate);
    set_view_hand(state.hand);    
    set_view_table(state.table);
    set_view_text_stage(state.text_stage);
    set_view_scores(state.scores);
};

var set_view_at_cleanedtable_msg = function(newstate){
// set game when a cleanedtable msg is received from server
    var state = set_game_at_cleanedtable_msg(newstate);    
console.log('Set view at cleantable, state:');
console.log(state);    
    //set_view_hand(state.hand);
    set_view_text_stage(state.text_stage);
    set_view_scores(state.scores);
    set_view_table(state.temptable);
    setTimeout(function (){set_view_table(state.table);},3000);
};

var set_view_at_finalresults_msg = function(newstate){
// set game when a finalresults msg is received from server
    var state = set_game_at_finalresults_msg(newstate);    
console.log('Set view at finalresults, state:');
console.log(state);    
    set_view_text_stage(newstate.results);
    set_view_scores(state.scores);
    set_view_table(state.temptable);
    setTimeout(function (){set_view_table(state.table);},3000);
};

var updateGame = function(newstate) {
 // registering update functions
 // would be more elegant in as a map?   
    if (newstate.instruction === "Initialise"){
console.log('Initialisation update')
        set_view_at_initialise(newstate);    
    }
    else if (newstate.instruction === "Newjoiner"){
console.log('Newjoiner update')
        set_view_at_newjoiner_msg(newstate);    
    }
    else if (newstate.instruction === "Refresh"){
console.log('Refresh update')
        set_view_at_refresh_msg(newstate);    
    }
    else if (newstate.instruction === "Deal"){
console.log('Deal')
        set_view_at_deal_msg(newstate);
    } 
    else if (newstate.instruction === "Exchange"){
console.log('Exchange')
        set_view_at_exchange_msg(newstate);
    }
    else if (newstate.instruction === "Placedcard"){
console.log('Placedcard')
        set_view_at_placecard_msg(newstate);
    }
    else if (newstate.instruction === "CleanedTable"){
console.log('CleanedTable')
        set_view_at_cleanedtable_msg(newstate);
    }
    else if (newstate.instruction === "FinalResults"){
console.log('FinalResults')
        set_view_at_finalresults_msg(newstate);
    }
    else {
console.log('Undefined update')
    }

};
  
openChannel = function() {
// setting up channel to server        
    var channel = new goog.appengine.Channel(token);
    var handler = {
      'onopen': onOpened,
      'onmessage': onMessage,
      'onerror': function() { console.log("Channel: onerror."); },
      'onclose': function() { console.log("Channel: onclose."); }
    };
    var socket = channel.open(handler);
    socket.onopen = onOpened;
    socket.onmessage = onMessage;
};

onOpened = function() {    
// function to be called when a channel is opened

    // let other players know about joining
console.log('State before joining message sent');
console.log(state);
console.log('initial_message');
console.log(initial_message);
    //var newstate = JSON.parse(initial_message.data);
    //sendMessage('/joined','',newstate.key); 
    //updateGame(newstate);
    onMessage(initial_message);
console.log("State after opened.");
console.log(state);
    set_listeners();
};

      
onMessage = function(m) {
// function to be called when a message is received from server
// processing a message from sever
    newstate = JSON.parse(m.data);    
console.log('Message received:');
console.log(newstate);
     updateGame(newstate);    
};

sendMessage = function(path, other) {
// send message to server
    path += '?k=' + state.key;    
    if (typeof(other) !== 'undefined')
        path += other;        
    var xhr = new XMLHttpRequest();
    xhr.open('POST', path, true);
    xhr.send();
    console.log('Message sent: ' + path);
};


initialize = function() {   
// initialisation -- opens channel to server         
    openChannel();                
};








