# README #

This is a implementation of a web-based small-scale four-player card game called Queen of Spades. The project is written for the Google App Engine. 

Note that this is an experimental version only with the aim of exploring and learning from experience. See some known issues under "Limitations" and under "Future plans". These issues are likely to be addressed in a separate version. 

### CONTENTS ###

* app.yaml -- project definitions
* main.py -- server side code implemented in python using google.app.engine.api, webapp2, jinja2
* game.html -- template of the user interface
* js/cardUrls.js -- collection of urls where the images are stored 
* js/control.js -- mediates between game.js and view.js
* js/game.js -- defines the logic of the game an individual player's point of view
* js/view.js -- defines the look of the user interface
* js/demo.js -- defines some intermediate states of the game for testing
* css/main.css -- some definitions

### LIMITATIONS ###

The application is designed and implemented as a small scale game that can handle only a few simultaneous games at a time (in return it does not reaches the free usage quota limits of the google app engine). In particular, it does not use the datastore but only the memcache. 

### FUTURE PLANS ###

In this version, the logic of the game is mostly handled on the client side, and it is assumed that the client would not fiddle with the application, send cheating messages to the server, etc., therefore in order to make the game safer and more robust,

* the server is to be made stricter on validating messages,
* the logic implementation is to be migrated to the server side. 

Other plans:

* use jQuery.param to define the client's message in a more elegant way
* use jQuery UI to improve the user interface
* and probably some more